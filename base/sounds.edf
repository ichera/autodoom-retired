//
// EDF for Eternity Engine v3.40.50
//
// This file contains sound definitions. Note that some fields in the
// sound block have very special semantics, so you should read the docs
// before you mess with this at all. Confusion is bound to result otherwise.
//

// Include gamemode-specific sounds --------------------------------------------

// Doom : DeHackEd nums 1 - 108, 125

includeifenabled("doom/sounds.edf", "DOOM")

// Heretic : DeHackEd nums 300 - 397

includeifenabled("heretic/sounds.edf", "HERETIC")

// MBF sounds ------------------------------------------------------------------

// killough 11/98: dog sounds

sound dgsit  { pitchvariance Doom; priority  98; dehackednum 109 }
sound dgatk  { pitchvariance Doom; priority  70; dehackednum 110 }
sound dgact  { pitchvariance Doom; priority 120; dehackednum 111 }
sound dgdth  { pitchvariance Doom; priority  70; dehackednum 112 }
sound dgpain { pitchvariance Doom; priority  96; dehackednum 113 }

// Eternity Engine sounds ------------------------------------------------------

sound eefly
{
  priority      120
  dehackednum   114
  clipping_dist 512
  close_dist    64
}

sound gloop  { pitchvariance Doom; priority 100; dehackednum 115 }
sound thundr { pitchvariance Doom; priority  96; dehackednum 116 }
sound muck   { pitchvariance Doom; priority 100; dehackednum 117 }

sound plfeet
{
  pitchvariance Doom
  priority      96
  dehackednum   118
  singularity   sg_oof
  skinindex     sk_plfeet
}

sound plfall
{
  pitchvariance Doom
  priority      96
  dehackednum   119
  skinindex     sk_plfall
}

sound fallht
{
  pitchvariance Doom
  priority      96
  dehackednum   120
  skinindex     sk_fallht
}

sound burn { pitchvariance Doom; priority 32; dehackednum 121 }

sound eehtsz
{ 
  pitchvariance Doom
  priority      100
  dehackednum   122
  clipping_dist 640
  close_dist    96
}

sound eedrip
{
  pitchvariance Doom
  priority      100
  dehackednum   123
  clipping_dist 640
  close_dist    96
}

sound Earthquake
{
  lump        "quake2";
  dehackednum 124
}

// NOTE: DeHackEd num 125 == plwdth in doom/sounds.edf


// Default Sector Sound Sequences ----------------------------------------------

// Door Opening Sequences

soundsequence EEDoorOpenNormal
{
  commands
  @"
    play EE_DoorOpen
    nostopcutoff
  "@
}

soundsequence EEDoorOpenBlazing
{
  commands
  @"
    play EE_BDoorOpen
    nostopcutoff
  "@
}

// Note: All gamemode sequences will be kept here since they are related.

// DOOM Door Close Sequences

ifgametype(DOOM)

soundsequence EEDoorCloseNormal
{
  commands
  @"
    play EE_DoorClose
    nostopcutoff
  "@
}

soundsequence EEDoorCloseBlazing
{
  commands
  @"
    play EE_BDoorClose
    nostopcutoff
  "@
}

// Note: This sequence is only used in DOOM when comp_blazing is enabled.
// It makes the (apparently) erroneous double-closing sound.

soundsequence EEDoorCloseBlazingComp
{
  commands
  @"
    play EE_BDoorClose
    stopsound EE_BDoorClose
  "@
}

endif()

// Heretic Door Close Sequences

ifgametype(HERETIC)

soundsequence EEDoorCloseNormal
{
  commands
  @"
    play EE_DoorOpen
    stopsound EE_DoorClose
  "@
}

soundsequence EEDoorCloseBlazing
{
  commands
  @"
    play EE_BDoorOpen
    stopsound EE_BDoorClose
  "@
}

endif()

// Plat Sequences

// This is used by most plat types, like the ordinary down-wait-up-stay type
soundsequence EEPlatNormal
{
  commands
  @"
    play EE_PlatStart
    stopsound EE_PlatStop
  "@
}

// This is used by raise-and-change plat types.
soundsequence EEPlatRaise
{
  commands
  @"
    playloop EE_PlatMove 8
    stopsound EE_PlatStop
  "@
}

// This may seem pointless, but defining it allows the silent behavior of 
// instant move plats to be overridden by redefining this sequence.
soundsequence EEPlatSilent
{
}

// Ceiling Sequences

// Used by most ceiling actions
soundsequence EECeilingNormal
{
  commands
  @"
    playloop EE_FCMove 8
  "@
}

// Used by "silent" crushers that aren't really silent
soundsequence EECeilingSemiSilent
{
  commands
  @"
    stopsound EE_PlatStop
  "@
}

// Like above for plats, but for ceilings.
soundsequence EECeilingSilent
{
}

// Floor Sequences

soundsequence EEFloor
{
  commands
  @"
    playloop EE_FCMove 8
    stopsound EE_PlatStop
  "@
}

